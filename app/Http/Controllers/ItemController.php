<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Item;
class ItemController extends Controller
{
    public function add()
    {
        return view('add_item');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'name' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric'
        ]);
        
        $input_data=$request->all();
        Item::create($input_data);
        return redirect('/home');
    }
}
