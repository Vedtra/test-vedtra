<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $table = 'invoice';
    protected $fillable=['id','company_id','item_id','qty'];

    public function company(){
    	return $this->belongsTo(Company::class,"company_id");
    }

    public function item(){
    	return $this->belongsTo(Item::class,"item_id");
    }
}
