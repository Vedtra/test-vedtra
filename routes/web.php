<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/add-company', 'CompanyController@add');
Route::get('/add-invoice', 'InvoiceController@add');
Route::get('/add-item', 'ItemController@add');
Route::get('/export', 'InvoiceController@exportCsv');
Route::post('/add-company/store', 'CompanyController@store');
Route::post('/add-invoice/store', 'InvoiceController@store');
Route::post('/add-item/store', 'ItemController@store');
