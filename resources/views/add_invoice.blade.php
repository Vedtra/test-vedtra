@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah transaksi</div>

                <div class="card-body">
                <a href="/home" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
                    <form method="post" action="/add-invoice/store">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Nama Perusahaan</label>
                            <select class="form-control select" name="company_id" required="required">
                                <option value="">Select</option>
                                @foreach($company as $comp)
                                   <option value="{{ $comp->id }}">{{ $comp->name }}</option>
                                 @endforeach
                             </select>

                            @if($errors->has('company_id'))
                                <div class="text-danger">
                                    {{ $errors->first('company_id')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>Nama Barang</label>
                            <select class="form-control select" name="item_id" required="required">
                                <option value="">Select</option>
                                @foreach($item as $data)
                                   <option value="{{ $data->id }}">{{ $data->name }}</option>
                                 @endforeach
                             </select>

                            @if($errors->has('item_id'))
                                <div class="text-danger">
                                    {{ $errors->first('item_id')}}
                                </div>
                            @endif

                        </div>
                        
                        <div class="form-group">
                            <label>Total Barang</label>
                            <input type="number" name="qty" class="form-control">

                            @if($errors->has('qty'))
                                <div class="text-danger">
                                    {{ $errors->first('qty')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
    
    $(document).ready(function(){
      $('.select').select2();
    });
  </script>
