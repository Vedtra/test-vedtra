@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah perusahaan</div>

                <div class="card-body">
                <a href="/home" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
                    <form method="post" action="/add-company/store">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Kode Perusahaan</label>
                            <input type="text" name="code" class="form-control" placeholder="Kode Perusahaan .." required="required">

                            @if($errors->has('code'))
                                <div class="text-danger">
                                    {{ $errors->first('code')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>Nama Perusahaan</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama perusahaan ..">

                            @if($errors->has('name'))
                                <div class="text-danger">
                                    {{ $errors->first('name')}}
                                </div>
                            @endif

                        </div>
                        

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
