@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Data perusahaan</div>

                <div class="card-body">
                <a href="add-company" class="btn btn-primary">Input Perusahaan</a>
                <br>
                <br>
                <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Code</th>
                                <th>Nama perusahaan</th>
                            </tr>
                        </thead>
                        <tbody>
                        @php $no = 1; @endphp
                        @foreach($company as $c)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $c->code }}</td>
                                <td>{{ $c->name }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            <div class="card">
                <div class="card-header">Data Transaksi</div>

                <div class="card-body">
                    <a href="add-invoice" class="btn btn-primary">Input Transaksi</a>
                    <br>
                    <br>
                    <a href="export" class="btn btn-primary">Export data</a>
                    <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Nama Barang</th>
                                    <th>Total Barang</th>
                                    <th>Harga Barang</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php $no = 1; @endphp
                            @foreach($invoice as $i)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $i->company->name }}</td>
                                    <td>{{ $i->item->name }}</td>
                                    <td>{{ $i->qty }}</td>
                                    <td>{{ 'Rp '.number_format($i->item->price,2,".",".") }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
            <div class="card">
                <div class="card-header">Data Barang</div>

                <div class="card-body">
                    <a href="add-item" class="btn btn-primary">Input Barang</a>
                    <br>
                    <br>
                    <table class="table table-bordered table-hover table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Barang</th>
                                    <th>Harga</th>
                                    <th>Stock</th>
                                </tr>
                            </thead>
                            <tbody>
                            @php $no = 1; @endphp
                            @foreach($item as $it)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $it->name }}</td>
                                    <td>{{ 'Rp '.number_format($it->price,2,".",".") }}</td>
                                    <td>{{ $it->stock }}</td>
                                    
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
