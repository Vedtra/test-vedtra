@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Tambah Barang</div>

                <div class="card-body">
                <a href="/home" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
                    <form method="post" action="/add-item/store">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input type="text" name="name" class="form-control" placeholder="Nama Barang .." required="required">

                            @if($errors->has('name'))
                                <div class="text-danger">
                                    {{ $errors->first('name')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>Harga Barang</label>
                            <input type="number" name="price" class="form-control">

                            @if($errors->has('price'))
                                <div class="text-danger">
                                    {{ $errors->first('price')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>Stock Barang</label>
                            <input type="number" name="stock" class="form-control">

                            @if($errors->has('stock'))
                                <div class="text-danger">
                                    {{ $errors->first('stock')}}
                                </div>
                            @endif

                        </div>
                        

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
