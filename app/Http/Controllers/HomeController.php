<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Invoice;
use App\Item;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $company = Company::all();
        $invoice = Invoice::all();
        $item = Item::all();
        return view('home', ['company' => $company,'invoice' => $invoice,'item' => $item]);
    }
}
