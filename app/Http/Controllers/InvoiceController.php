<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\Company;
use App\Item;
class InvoiceController extends Controller
{
    public function add()
    {
        $company = Company::all();
        $item = Item::all();
        return view('add_invoice',['company' => $company,'item' => $item]);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'company_id' => 'required',
            'item_id' => 'required',
            'qty' => 'required|numeric'
        ]);
        
        $input_data=$request->all();
        Invoice::create($input_data);
        return redirect('/home');
    }

    public function exportCsv(Request $request)
    {
    $fileName = 'data_transaksi.csv';
    $invoices = invoice::all();

            $headers = array(
                "Content-type"        => "text/csv",
                "Content-Disposition" => "attachment; filename=$fileName",
                "Pragma"              => "no-cache",
                "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
                "Expires"             => "0"
            );

            $columns = array('Tanggal Input', 'Nama Perusahaan', 'Nama Barang', 'Total Barang', 'Harga Barang','Grand Total','Sisa Barang');

            $callback = function() use($invoices, $columns) {
                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                foreach ($invoices as $invoice) {
                    $row['Tanggal Input']  = $invoice->created_at;
                    $row['Nama Perusahaan']    = $invoice->company->name;
                    $row['Nama Barang']    = $invoice->item->name;
                    $row['Total Barang']  = $invoice->qty;
                    $row['Harga Barang']  = $invoice->item->price;
                    $row['Grand Total']  = $invoice->item->price * $invoice->qty ;
                    $row['Sisa Barang']  = $invoice->item->stock;
                    fputcsv($file, array($row['Tanggal Input'], $row['Nama Perusahaan'], $row['Nama Barang'], $row['Total Barang'], $row['Harga Barang'],$row['Grand Total'],$row['Sisa Barang']));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }
}
