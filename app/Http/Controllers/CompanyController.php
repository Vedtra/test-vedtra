<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
class CompanyController extends Controller
{
    public function add()
    {
        return view('add_company');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
    		'code' => 'required',
    		'name' => 'required'
        ]);
        
        $input_data=$request->all();
        Company::create($input_data);
        return redirect('/home');
    }
}
